import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

const Filter = ({ onFilterChange }) => {
  const handleFilterChange = (filterName, value) => {
    onFilterChange(filterName, value);
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Estado"
        placeholderTextColor="#888"
        onChangeText={(value) => handleFilterChange('filtro1', value)}
      />
      <TextInput
        style={styles.input}
        placeholder="Municipio"
        placeholderTextColor="#888"
        onChangeText={(value) => handleFilterChange('filtro2', value)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 12,
    marginRight: 10,
    fontSize: 16,
    color: '#333',
    backgroundColor:'white',
    width: 130, // Aumente o valor para aumentar a largura do campo
  },
});

export default Filter;
