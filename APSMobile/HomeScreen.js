import React, { useState } from 'react';
import { Button, StyleSheet, View, Platform, TouchableOpacity,Text } from 'react-native';
import Logo from './Logo';
import Filter from './Filter';
import data from './Data';



const HomeScreen = ({ navigation }) => {
  const [filters, setFilters] = useState({ filtro1: '', filtro2: '' });

  const handleFilterChange = (filterName, value) => {
    setFilters({ ...filters, [filterName]: value });
  };



  const handleApplyFilter = () => {

    const filteredData = data.filter((item) => {
      const match = item.state.trim() === filters.filtro1.trim() && item.municipality.trim() === filters.filtro2.trim();

      console.log('Item:', item);
      console.log('Match:', match);
      return match;
    });
    
        
  
    console.log('Valores dos filtros:', filters);
    console.log('Dados filtrados:', filteredData);

    if (filteredData.length === 0) {
      console.log('Nenhum dado encontrado com os filtros selecionados.');
      return;
    }
    navigation.navigate('Chart', { filteredData: filteredData });
  };
  
  

  const handleClearFilter = () => {
    setFilters({ filtro1: '', filtro2: '' });

  };
  
  return (
    <View style={styles.container}>
      <View style={styles.background}></View>
      <Logo style={styles.logo}/>
      <Text style={styles.title}>Índice</Text>
      <Text style={styles.title2}>de desmatamento no</Text>
      <Text style={styles.title3}>Brasil</Text>
      <Filter onFilterChange={handleFilterChange} />
      <View style={styles.buttonContainer}>
      <TouchableOpacity style={styles.button} onPress={handleApplyFilter}>
        <Text style={styles.buttonText}>Consultar</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttonLimpar} onPress={handleClearFilter}>
        <Text style={styles.buttonTextLimpar}>Limpar Filtro</Text>
      </TouchableOpacity>
</View>
      
      
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  background: {
    position: 'absolute',
    top: 120,
    left:35,
    right:35,
    bottom: 120,
    backgroundColor: 'lightblue',
    opacity: 0.5,
    borderRadius: 50,
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'green',
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowOffset: { width: 2, height: 3 },
    textShadowRadius: 2,
  },
  title2: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowOffset: { width: 2, height: 3 },
    textShadowRadius: 2,
  },
  title3: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'green',
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowOffset: { width: 2, height: 3 },
    textShadowRadius: 2,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  buttonExit: {
    backgroundColor: 'red',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 'auto',
    marginRight: 40,
    alignSelf: 'flex-end',
  },
  buttonLimpar: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
  },
  buttonTextExit: {
    color: 'white',
    fontSize: 18,
  },
  buttonTextLimpar: {
    color: 'white',
    fontSize: 18,
  }
});

export default HomeScreen;
