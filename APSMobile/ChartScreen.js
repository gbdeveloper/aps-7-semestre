import React from 'react';
import { Dimensions, StyleSheet, View, Text } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import data from './Data';

const ChartScreen = ({ route }) => {
  const { filteredData } = route.params;

  const chartData = filteredData.map((item) => ({
    y: Number(item.deforestation),
  }));

    const yMin = 10;
    const yMax =5000;

 

  if (chartData.length === 0) {
    return (
      <View style={styles.container}>
        <Text>Nenhum dado encontrado com os filtros selecionados.</Text>
      </View>
    );
  }

  console.log(chartData);
  console.log(yMin, yMax);

  return (
    <View style={styles.container}>
      <View style={styles.background}></View>
      <Text style={styles.title}>Índice dos últimos 5 anos</Text>
      <LineChart
        data={{
          labels: ["2018", "2019", "2020", "2021", "2022"],
          datasets: [
            {
              data: [
                chartData[0].y,
                chartData[1].y,
                chartData[2].y,
                chartData[3].y,
                chartData[4].y
              ]
            }
          ]
        }}
        width={Dimensions.get("window").width}
        height={520}
        yAxisSuffix="km²"
        yAxisInterval={1}
        chartConfig={{
          backgroundColor: "black",
          backgroundGradientFrom: "green",
          backgroundGradientTo: "green",
          decimalPlaces: 1,
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          },
          propsForDots: {
            r: "6",
            strokeWidth: "2",
            stroke: "black"
          }
        }}
        bezier
        style={{
          marginTop:22,
          marginVertical: 7,
          borderRadius: 10,
          marginLeft:8,
          marginRight:8
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 8,
    color: 'black',
    textShadowColor: 'rgba(0, 0, 0, 0.3)',
    textShadowOffset: { width: 6, height: 3 },
    textShadowRadius: 2,
  },
  
  background: {
    position: 'absolute',
    top: 1,
    left:1,
    right:1,
    bottom: 1,
    backgroundColor: 'lightblue',
    opacity: 0.5,
    borderRadius: 2,
  },
  chart: {
    marginVertical: 8,
    borderRadius: 16,
  },
});

export default ChartScreen;
