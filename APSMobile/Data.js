// src/utils/data.js

const data = [
  {year : '2018', deforestation: 179.9, municipality: 'Anadia', state: 'Alagoas' },
  {year : '2019', deforestation: 150.9, municipality: 'Anadia', state: 'Alagoas' },
  {year : '2020', deforestation: 122.9, municipality: 'Anadia', state: 'Alagoas' },
  {year : '2021', deforestation: 133.9, municipality: 'Anadia', state: 'Alagoas' },
  {year : '2022', deforestation: 200.9, municipality: 'Anadia', state: 'Alagoas' },
  {year : '2018', deforestation: 70.9, municipality: 'Uniao Paulista', state: 'Sao Paulo' },
  {year : '2019', deforestation: 70.9, municipality: 'Uniao Paulista', state: 'Sao Paulo' },
  {year : '2020', deforestation: 70.9, municipality: 'Uniao Paulista', state: 'Sao Paulo' },
  {year : '2021', deforestation: 70.9, municipality: 'Uniao Paulista', state: 'Sao Paulo' },
  {year : '2022', deforestation: 70.9, municipality: 'Uniao Paulista', state: 'Sao Paulo' },
  {year : '2018', deforestation: 195.6, municipality: 'Urania', state: 'Sao Paulo' },
  {year : '2019', deforestation: 270.6, municipality: 'Urania', state: 'Sao Paulo' },
  {year : '2020', deforestation: 300.6, municipality: 'Urania', state: 'Sao Paulo' },
  {year : '2021', deforestation: 135.6, municipality: 'Urania', state: 'Sao Paulo' },
  {year : '2022', deforestation: 194.6, municipality: 'Urania', state: 'Sao Paulo' },
  {year : '2018', deforestation: 136.0, municipality: 'Uru', state: 'Sao Paulo' },
  {year : '2019', deforestation: 136.0, municipality: 'Uru', state: 'Sao Paulo' },
  {year : '2020', deforestation: 136.0, municipality: 'Uru', state: 'Sao Paulo' },
  {year : '2021', deforestation: 136.0, municipality: 'Uru', state: 'Sao Paulo' },
  {year : '2022', deforestation: 136.0, municipality: 'Uru', state: 'Sao Paulo' },
  {year : '2018', deforestation: 293.3, municipality: 'Urupes', state: 'Sao Paulo' },
  {year : '2019', deforestation: 293.3, municipality: 'Urupes', state: 'Sao Paulo' },
  {year : '2020', deforestation: 293.3, municipality: 'Urupes', state: 'Sao Paulo' },
  {year : '2021', deforestation: 293.3, municipality: 'Urupes', state: 'Sao Paulo' },
  {year : '2022', deforestation: 293.3, municipality: 'Urupes', state: 'Sao Paulo' },
  {year : '2018', deforestation: 133.3, municipality: 'Valentim Gentil', state: 'Sao Paulo' },
  {year : '2019', deforestation: 133.3, municipality: 'Valentim Gentil', state: 'Sao Paulo' },
  {year : '2020', deforestation: 133.3, municipality: 'Valentim Gentil', state: 'Sao Paulo' },
  {year : '2021', deforestation: 133.3, municipality: 'Valentim Gentil', state: 'Sao Paulo' },
  {year : '2022', deforestation: 133.3, municipality: 'Valentim Gentil', state: 'Sao Paulo' },
  {year : '2018', deforestation: 124.4, municipality: 'Valinhos', state: 'Sao Paulo' },
  {year : '2019', deforestation: 124.4, municipality: 'Valinhos', state: 'Sao Paulo' },
  {year : '2020', deforestation: 124.4, municipality: 'Valinhos', state: 'Sao Paulo' },
  {year : '2021', deforestation: 124.4, municipality: 'Valinhos', state: 'Sao Paulo' },
  {year : '2022', deforestation: 124.4, municipality: 'Valinhos', state: 'Sao Paulo' },
  {year : '2018', deforestation: 753.2, municipality: 'Valparaiso', state: 'Sao Paulo' },
  {year : '2019', deforestation: 753.2, municipality: 'Valparaiso', state: 'Sao Paulo' },
  {year : '2020', deforestation: 753.2, municipality: 'Valparaiso', state: 'Sao Paulo' },
  {year : '2021', deforestation: 753.2, municipality: 'Valparaiso', state: 'Sao Paulo' },
  {year : '2022', deforestation: 753.2, municipality: 'Valparaiso', state: 'Sao Paulo' },
  {year : '2018', deforestation: 113.3, municipality: 'Vargem', state: 'Sao Paulo' },
  {year : '2019', deforestation: 113.3, municipality: 'Vargem', state: 'Sao Paulo' },
  {year : '2020', deforestation: 113.3, municipality: 'Vargem', state: 'Sao Paulo' },
  {year : '2021', deforestation: 113.4, municipality: 'Vargem', state: 'Sao Paulo' },
  {year : '2022', deforestation: 113.4, municipality: 'Vargem', state: 'Sao Paulo' },
  {year : '2018', deforestation: 32.4, municipality: 'Vargem Grande Paulista', state: 'Sao Paulo' },
  {year : '2019', deforestation: 32.4, municipality: 'Vargem Grande Paulista', state: 'Sao Paulo' },
  {year : '2020', deforestation: 32.4, municipality: 'Vargem Grande Paulista', state: 'Sao Paulo' },
  {year : '2021', deforestation: 32.4, municipality: 'Vargem Grande Paulista', state: 'Sao Paulo' },
  {year : '2022', deforestation: 32.5, municipality: 'Vargem Grande Paulista', state: 'Sao Paulo' },
  {year : '2018', deforestation: 172.6, municipality: 'Vargem Grande do Sul', state: 'Sao Paulo' },
  {year : '2019', deforestation: 172.7, municipality: 'Vargem Grande do Sul', state: 'Sao Paulo' },
  {year : '2020', deforestation: 172.8, municipality: 'Vargem Grande do Sul', state: 'Sao Paulo' },
  {year : '2021', deforestation: 172.8, municipality: 'Vargem Grande do Sul', state: 'Sao Paulo' },
  {year : '2022', deforestation: 172.8, municipality: 'Vargem Grande do Sul', state: 'Sao Paulo' },
  {year : '2018', deforestation: 29.2, municipality: 'Varzea Paulista', state: 'Sao Paulo' },
  {year : '2019', deforestation: 29.2, municipality: 'Varzea Paulista', state: 'Sao Paulo' },
  {year : '2020', deforestation: 29.2, municipality: 'Varzea Paulista', state: 'Sao Paulo' },
  {year : '2021', deforestation: 29.2, municipality: 'Varzea Paulista', state: 'Sao Paulo' },
  {year : '2022', deforestation: 29.2, municipality: 'Varzea Paulista', state: 'Sao Paulo' },
  {year : '2018', deforestation: 201.3, municipality: 'Vera Cruz', state: 'Sao Paulo' },
  {year : '2019', deforestation: 201.3, municipality: 'Vera Cruz', state: 'Sao Paulo' },
  {year : '2020', deforestation: 201.3, municipality: 'Vera Cruz', state: 'Sao Paulo' },
  {year : '2021', deforestation: 201.4, municipality: 'Vera Cruz', state: 'Sao Paulo' },
  {year : '2022', deforestation: 201.4, municipality: 'Vera Cruz', state: 'Sao Paulo' },
  {year : '2018', deforestation: 67.0, municipality: 'Vinhedo', state: 'Sao Paulo' },
  {year : '2019', deforestation: 67.0, municipality: 'Vinhedo', state: 'Sao Paulo' },
  {year : '2020', deforestation: 67.0, municipality: 'Vinhedo', state: 'Sao Paulo' },
  {year : '2021', deforestation: 67.0, municipality: 'Vinhedo', state: 'Sao Paulo' },
  {year : '2022', deforestation: 67.0, municipality: 'Vinhedo', state: 'Sao Paulo' },
  {year : '2018', deforestation: 127.4, municipality: 'Viradouro', state: 'Sao Paulo' },
  {year : '2019', deforestation: 127.4, municipality: 'Viradouro', state: 'Sao Paulo' },
  {year : '2020', deforestation: 127.4, municipality: 'Viradouro', state: 'Sao Paulo' },
  {year : '2021', deforestation: 127.4, municipality: 'Viradouro', state: 'Sao Paulo' },
  {year : '2022', deforestation: 127.4, municipality: 'Viradouro', state: 'Sao Paulo' },
  {year : '2018', deforestation: 87.9, municipality: 'Vista Alegre do Alto', state: 'Sao Paulo' },
  {year : '2019', deforestation: 87.9, municipality: 'Vista Alegre do Alto', state: 'Sao Paulo' },
  {year : '2020', deforestation: 87.9, municipality: 'Vista Alegre do Alto', state: 'Sao Paulo' },
  {year : '2021', deforestation: 87.9, municipality: 'Vista Alegre do Alto', state: 'Sao Paulo' },
  {year : '2022', deforestation: 87.9, municipality: 'Vista Alegre do Alto', state: 'Sao Paulo' },
  {year : '2018', deforestation: 46.1, municipality: 'Vitoria Brasil', state: 'Sao Paulo' },
  {year : '2019', deforestation: 46.1, municipality: 'Vitoria Brasil', state: 'Sao Paulo' },
  {year : '2020', deforestation: 46.1, municipality: 'Vitoria Brasil', state: 'Sao Paulo' },
  {year : '2021', deforestation: 46.1, municipality: 'Vitoria Brasil', state: 'Sao Paulo' },
  {year : '2022', deforestation: 46.1, municipality: 'Vitoria Brasil', state: 'Sao Paulo' },
  {year : '2018', deforestation: 131.8, municipality: 'Votorantim', state: 'Sao Paulo' },
  {year : '2019', deforestation: 131.8, municipality: 'Votorantim', state: 'Sao Paulo' },
  {year : '2020', deforestation: 131.8, municipality: 'Votorantim', state: 'Sao Paulo' },
  {year : '2021', deforestation: 131.8, municipality: 'Votorantim', state: 'Sao Paulo' },
  {year : '2022', deforestation: 131.9, municipality: 'Votorantim', state: 'Sao Paulo' },
  {year : '2018', deforestation: 381.5, municipality: 'Votuporanga', state: 'Sao Paulo' },
  {year : '2019', deforestation: 381.5, municipality: 'Votuporanga', state: 'Sao Paulo' },
  {year : '2020', deforestation: 381.5, municipality: 'Votuporanga', state: 'Sao Paulo' },
  {year : '2021', deforestation: 381.5, municipality: 'Votuporanga', state: 'Sao Paulo' },
  {year : '2022', deforestation: 381.5, municipality: 'Votuporanga', state: 'Sao Paulo' },
  {year : '2018', deforestation: 279.3, municipality: 'Zacarias', state: 'Sao Paulo' },
  {year : '2019', deforestation: 279.3, municipality: 'Zacarias', state: 'Sao Paulo' },
  {year : '2020', deforestation: 279.6, municipality: 'Zacarias', state: 'Sao Paulo' },
  {year : '2021', deforestation: 279.7, municipality: 'Zacarias', state: 'Sao Paulo' },
  {year : '2022', deforestation: 279.7, municipality: 'Zacarias', state: 'Sao Paulo' },
  {year : '2018', deforestation: 176.1, municipality: 'Aquidaba', state: 'Sergipe' },
  {year : '2019', deforestation: 176.1, municipality: 'Aquidaba', state: 'Sergipe' },
  {year : '2020', deforestation: 176.4, municipality: 'Aquidaba', state: 'Sergipe' },
  {year : '2021', deforestation: 176.5, municipality: 'Aquidaba', state: 'Sergipe' },
  {year : '2022', deforestation: 176.5, municipality: 'Aquidaba', state: 'Sergipe' },
  {year : '2018', deforestation: 124.0, municipality: 'Aracaju', state: 'Sergipe' },
  {year : '2019', deforestation: 124.0, municipality: 'Aracaju', state: 'Sergipe' },
  {year : '2020', deforestation: 124.1, municipality: 'Aracaju', state: 'Sergipe' },
  {year : '2021', deforestation: 124.1, municipality: 'Aracaju', state: 'Sergipe' },
  {year : '2022', deforestation: 124.1, municipality: 'Aracaju', state: 'Sergipe' },
  {year : '2018', deforestation: 167.2, municipality: 'Araua', state: 'Sergipe' },
  {year : '2019', deforestation: 167.4, municipality: 'Araua', state: 'Sergipe' },
  {year : '2020', deforestation: 168.4, municipality: 'Araua', state: 'Sergipe' },
  {year : '2021', deforestation: 168.4, municipality: 'Araua', state: 'Sergipe' },
  {year : '2022', deforestation: 168.8, municipality: 'Araua', state: 'Sergipe' },
  {year : '2018', deforestation: 66.8, municipality: 'Areia Branca', state: 'Sergipe' },
  {year : '2019', deforestation: 66.8, municipality: 'Areia Branca', state: 'Sergipe' },
  {year : '2020', deforestation: 68.4, municipality: 'Areia Branca', state: 'Sergipe' },
  {year : '2021', deforestation: 68.4, municipality: 'Areia Branca', state: 'Sergipe' },
  {year : '2022', deforestation: 68.5, municipality: 'Areia Branca', state: 'Sergipe' },
  {year : '2018', deforestation: 50.8, municipality: 'Barra dos Coqueiros', state: 'Sergipe' },
  {year : '2019', deforestation: 50.8, municipality: 'Barra dos Coqueiros', state: 'Sergipe' },
  {year : '2020', deforestation: 50.9, municipality: 'Barra dos Coqueiros', state: 'Sergipe' },
  {year : '2021', deforestation: 51.0, municipality: 'Barra dos Coqueiros', state: 'Sergipe' },
  {year : '2022', deforestation: 51.0, municipality: 'Barra dos Coqueiros', state: 'Sergipe' },
  {year : '2018', deforestation: 206.0, municipality: 'Boquim', state: 'Sergipe' },
  {year : '2019', deforestation: 206.0, municipality: 'Boquim', state: 'Sergipe' },
  {year : '2020', deforestation: 206.2, municipality: 'Boquim', state: 'Sergipe' },
  {year : '2021', deforestation: 206.2, municipality: 'Boquim', state: 'Sergipe' },
  {year : '2022', deforestation: 206.2, municipality: 'Boquim', state: 'Sergipe' },
  {year : '2018', deforestation: 76.8, municipality: 'Brejo Grande', state: 'Sergipe' },
  {year : '2019', deforestation: 76.8, municipality: 'Brejo Grande', state: 'Sergipe' },
  {year : '2020', deforestation: 77.4, municipality: 'Brejo Grande', state: 'Sergipe' },
  {year : '2021', deforestation: 78.0, municipality: 'Brejo Grande', state: 'Sergipe' },
  {year : '2022', deforestation: 78.0, municipality: 'Brejo Grande', state: 'Sergipe' },
  {year : '2018', deforestation: 55.2, municipality: 'Campo do Brito', state: 'Sergipe' },
  {year : '2019', deforestation: 55.3, municipality: 'Campo do Brito', state: 'Sergipe' },
  {year : '2020', deforestation: 56.3, municipality: 'Campo do Brito', state: 'Sergipe' },
  {year : '2021', deforestation: 56.3, municipality: 'Campo do Brito', state: 'Sergipe' },
  {year : '2022', deforestation: 56.6, municipality: 'Campo do Brito', state: 'Sergipe' },
  {year : '2018', deforestation: 14.7, municipality: 'Canhoba', state: 'Sergipe' },
  {year : '2019', deforestation: 14.7, municipality: 'Canhoba', state: 'Sergipe' },
  {year : '2020', deforestation: 14.7, municipality: 'Canhoba', state: 'Sergipe' },
  {year : '2021', deforestation: 14.7, municipality: 'Canhoba', state: 'Sergipe' },
  {year : '2022', deforestation: 14.7, municipality: 'Canhoba', state: 'Sergipe' },
  {year : '2018', deforestation: 397.5, municipality: 'Capela', state: 'Sergipe' },
  {year : '2019', deforestation: 397.6, municipality: 'Capela', state: 'Sergipe' },
  {year : '2020', deforestation: 399.2, municipality: 'Capela', state: 'Sergipe' },
  {year : '2021', deforestation: 399.3, municipality: 'Capela', state: 'Sergipe' },
  {year : '2022', deforestation: 400.3, municipality: 'Capela', state: 'Sergipe' },
  {year : '2018', deforestation: 43.3, municipality: 'Carmopolis', state: 'Sergipe' },
  {year : '2019', deforestation: 43.3, municipality: 'Carmopolis', state: 'Sergipe' },
  {year : '2020', deforestation: 43.3, municipality: 'Carmopolis', state: 'Sergipe' },
  {year : '2021', deforestation: 43.3, municipality: 'Carmopolis', state: 'Sergipe' },
  {year : '2022', deforestation: 43.3, municipality: 'Carmopolis', state: 'Sergipe' },
  {year : '2018', deforestation: 39.1, municipality: 'Cedro de Sao Joao', state: 'Sergipe' },
  {year : '2019', deforestation: 39.1, municipality: 'Cedro de Sao Joao', state: 'Sergipe' },
  {year : '2020', deforestation: 39.2, municipality: 'Cedro de Sao Joao', state: 'Sergipe' },
  {year : '2021', deforestation: 39.2, municipality: 'Cedro de Sao Joao', state: 'Sergipe' },
  {year : '2022', deforestation: 39.2, municipality: 'Cedro de Sao Joao', state: 'Sergipe' },
  {year : '2018', deforestation: 218.2, municipality: 'Cristinapolis', state: 'Sergipe' },
  {year : '2019', deforestation: 218.3, municipality: 'Cristinapolis', state: 'Sergipe' },
  {year : '2020', deforestation: 218.8, municipality: 'Cristinapolis', state: 'Sergipe' },
  {year : '2021', deforestation: 218.9, municipality: 'Cristinapolis', state: 'Sergipe' },
  {year : '2022', deforestation: 218.9, municipality: 'Cristinapolis', state: 'Sergipe' },
  {year : '2018', deforestation: 97.5, municipality: 'Cumbe', state: 'Sergipe' },
  {year : '2019', deforestation: 97.6, municipality: 'Cumbe', state: 'Sergipe' },
  {year : '2020', deforestation: 97.6, municipality: 'Cumbe', state: 'Sergipe' },
  {year : '2021', deforestation: 98.2, municipality: 'Cumbe', state: 'Sergipe' },
  {year : '2022', deforestation: 98.3, municipality: 'Cumbe', state: 'Sergipe' },
  {year : '2018', deforestation: 80.6, municipality: 'Divina Pastora', state: 'Sergipe' },
  {year : '2019', deforestation: 80.6, municipality: 'Divina Pastora', state: 'Sergipe' },
  {year : '2020', deforestation: 80.6, municipality: 'Divina Pastora', state: 'Sergipe' },
  {year : '2021', deforestation: 80.6, municipality: 'Divina Pastora', state: 'Sergipe' },
  {year : '2022', deforestation: 81.1, municipality: 'Divina Pastora', state: 'Sergipe' },
  {year : '2018', deforestation: 425.2, municipality: 'Estancia', state: 'Sergipe' },
  {year : '2019', deforestation: 426.0, municipality: 'Estancia', state: 'Sergipe' },
  {year : '2020', deforestation: 428.2, municipality: 'Estancia', state: 'Sergipe' },
  {year : '2021', deforestation: 428.4, municipality: 'Estancia', state: 'Sergipe' },
  {year : '2022', deforestation: 428.7, municipality: 'Estancia', state: 'Sergipe' },
  {year : '2018', deforestation: 17.0, municipality: 'General Maynard', state: 'Sergipe' },
  {year : '2019', deforestation: 17.0, municipality: 'General Maynard', state: 'Sergipe' },
  {year : '2020', deforestation: 17.0, municipality: 'General Maynard', state: 'Sergipe' },
  {year : '2021', deforestation: 17.0, municipality: 'General Maynard', state: 'Sergipe' },
  {year : '2022', deforestation: 17.1, municipality: 'General Maynard', state: 'Sergipe' },
  {year : '2018', deforestation: 87.7, municipality: 'Gracho Cardoso', state: 'Sergipe' },
  {year : '2019', deforestation: 87.7, municipality: 'Gracho Cardoso', state: 'Sergipe' },
  {year : '2020', deforestation: 87.8, municipality: 'Gracho Cardoso', state: 'Sergipe' },
  {year : '2021', deforestation: 87.8, municipality: 'Gracho Cardoso', state: 'Sergipe' },
  {year : '2022', deforestation: 88.2, municipality: 'Gracho Cardoso', state: 'Sergipe' },
  {year : '2018', deforestation: 43.8, municipality: 'Ilha das Flores', state: 'Sergipe' },
  {year : '2019', deforestation: 43.8, municipality: 'Ilha das Flores', state: 'Sergipe' },
  {year : '2020', deforestation: 43.8, municipality: 'Ilha das Flores', state: 'Sergipe' },
  {year : '2021', deforestation: 43.8, municipality: 'Ilha das Flores', state: 'Sergipe' },
  {year : '2022', deforestation: 43.8, municipality: 'Ilha das Flores', state: 'Sergipe' },
  {year : '2018', deforestation: 210.7, municipality: 'Indiaroba', state: 'Sergipe' },
  {year : '2019', deforestation: 211.9, municipality: 'Indiaroba', state: 'Sergipe' },
  {year : '2020', deforestation: 213.5, municipality: 'Indiaroba', state: 'Sergipe' },
  {year : '2021', deforestation: 213.6, municipality: 'Indiaroba', state: 'Sergipe' },
  {year : '2022', deforestation: 213.7, municipality: 'Indiaroba', state: 'Sergipe' },
  {year : '2018', deforestation: 44.8, municipality: 'Itabaiana', state: 'Sergipe' },
  {year : '2019', deforestation: 44.8, municipality: 'Itabaiana', state: 'Sergipe' },
  {year : '2020', deforestation: 45.2, municipality: 'Itabaiana', state: 'Sergipe' },
  {year : '2021', deforestation: 45.2, municipality: 'Itabaiana', state: 'Sergipe' },
  {year : '2022', deforestation: 45.3, municipality: 'Itabaiana', state: 'Sergipe' },
  {year : '2018', deforestation: 146.7, municipality: 'Itabaianinha', state: 'Sergipe' },
  {year : '2019', deforestation: 147.0, municipality: 'Itabaianinha', state: 'Sergipe' },
  {year : '2020', deforestation: 147.6, municipality: 'Itabaianinha', state: 'Sergipe' },
  {year : '2021', deforestation: 147.6, municipality: 'Itabaianinha', state: 'Sergipe' },
  {year : '2022', deforestation: 147.6, municipality: 'Itabaianinha', state: 'Sergipe' },
  {year : '2018', deforestation: 39.7, municipality: 'Itabi', state: 'Sergipe' },
  {year : '2019', deforestation: 39.7, municipality: 'Itabi', state: 'Sergipe' },
  {year : '2020', deforestation: 39.9, municipality: 'Itabi', state: 'Sergipe' },
  {year : '2021', deforestation: 39.9, municipality: 'Itabi', state: 'Sergipe' },
  {year : '2022', deforestation: 39.9, municipality: 'Itabi', state: 'Sergipe' },
  {year : '2018', deforestation: 318.0, municipality: 'Japaratuba', state: 'Sergipe' },
  {year : '2019', deforestation: 318.0, municipality: 'Japaratuba', state: 'Sergipe' },
  {year : '2020', deforestation: 318.4, municipality: 'Japaratuba', state: 'Sergipe' },
  {year : '2021', deforestation: 318.5, municipality: 'Japaratuba', state: 'Sergipe' },
  {year : '2022', deforestation: 319.1, municipality: 'Japaratuba', state: 'Sergipe' },
  {year : '2018', deforestation: 366.4, municipality: 'Japoata', state: 'Sergipe' },
  {year : '2019', deforestation: 366.4, municipality: 'Japoata', state: 'Sergipe' },
  {year : '2020', deforestation: 366.9, municipality: 'Japoata', state: 'Sergipe' },
  {year : '2021', deforestation: 367.0, municipality: 'Japoata', state: 'Sergipe' },
  {year : '2022', deforestation: 367.5, municipality: 'Japoata', state: 'Sergipe' },
  {year : '2018', deforestation: 171.4, municipality: 'Lagarto', state: 'Sergipe' },
  {year : '2019', deforestation: 171.5, municipality: 'Lagarto', state: 'Sergipe' },
  {year : '2020', deforestation: 171.7, municipality: 'Lagarto', state: 'Sergipe' },
  {year : '2021', deforestation: 171.8, municipality: 'Lagarto', state: 'Sergipe' },
  {year : '2022', deforestation: 171.8, municipality: 'Lagarto', state: 'Sergipe' },
  {year : '2018', deforestation: 120.0, municipality: 'Laranjeiras', state: 'Sergipe' },
  {year : '2019', deforestation: 120.1, municipality: 'Laranjeiras', state: 'Sergipe' },
  {year : '2020', deforestation: 120.3, municipality: 'Laranjeiras', state: 'Sergipe' },
  {year : '2021', deforestation: 120.3, municipality: 'Laranjeiras', state: 'Sergipe' },
  {year : '2022', deforestation: 120.5, municipality: 'Laranjeiras', state: 'Sergipe' },
  {year : '2018', deforestation: 0.0, municipality: 'Macambira', state: 'Sergipe' },
  {year : '2019', deforestation: 0.0, municipality: 'Macambira', state: 'Sergipe' },
  {year : '2020', deforestation: 0.0, municipality: 'Macambira', state: 'Sergipe' },
  {year : '2021', deforestation: 0.0, municipality: 'Macambira', state: 'Sergipe' },
  {year : '2022', deforestation: 0.0, municipality: 'Macambira', state: 'Sergipe' },
  {year : '2018', deforestation: 14.8, municipality: 'Malhada dos Bois', state: 'Sergipe' },
  {year : '2019', deforestation: 14.8, municipality: 'Malhada dos Bois', state: 'Sergipe' },
  {year : '2020', deforestation: 14.8, municipality: 'Malhada dos Bois', state: 'Sergipe' },
  {year : '2021', deforestation: 14.8, municipality: 'Malhada dos Bois', state: 'Sergipe' },
  {year : '2022', deforestation: 14.9, municipality: 'Malhada dos Bois', state: 'Sergipe' },
  {year : '2018', deforestation: 70.9, municipality: 'Malhador', state: 'Sergipe' },
  {year : '2019', deforestation: 70.9, municipality: 'Malhador', state: 'Sergipe' },
  {year : '2020', deforestation: 71.9, municipality: 'Malhador', state: 'Sergipe' },
  {year : '2021', deforestation: 71.9, municipality: 'Malhador', state: 'Sergipe' },
  {year : '2022', deforestation: 72.1, municipality: 'Malhador', state: 'Sergipe' },
  {year : '2018', deforestation: 77.8, municipality: 'Maruim', state: 'Sergipe' },
  {year : '2019', deforestation: 77.8, municipality: 'Maruim', state: 'Sergipe' },
  {year : '2020', deforestation: 77.9, municipality: 'Maruim', state: 'Sergipe' },
  {year : '2021', deforestation: 77.9, municipality: 'Maruim', state: 'Sergipe' },
  {year : '2022', deforestation: 77.9, municipality: 'Maruim', state: 'Sergipe' },
  {year : '2018', deforestation: 26.7, municipality: 'Moita Bonita', state: 'Sergipe' },
  {year : '2019', deforestation: 26.7, municipality: 'Moita Bonita', state: 'Sergipe' },
  {year : '2020', deforestation: 27.1, municipality: 'Moita Bonita', state: 'Sergipe' },
  {year : '2021', deforestation: 27.1, municipality: 'Moita Bonita', state: 'Sergipe' },
  {year : '2022', deforestation: 27.1, municipality: 'Moita Bonita', state: 'Sergipe' },
  {year : '2018', deforestation: 54.8, municipality: 'Muribeca', state: 'Sergipe' },
  {year : '2019', deforestation: 54.8, municipality: 'Muribeca', state: 'Sergipe' },
  {year : '2020', deforestation: 55.0, municipality: 'Muribeca', state: 'Sergipe' },
  {year : '2021', deforestation: 55.0, municipality: 'Muribeca', state: 'Sergipe' },
  {year : '2022', deforestation: 55.0, municipality: 'Muribeca', state: 'Sergipe' },
  {year : '2018', deforestation: 206.6, municipality: 'Neopolis', state: 'Sergipe' },
  {year : '2019', deforestation: 206.7, municipality: 'Neopolis', state: 'Sergipe' },
  {year : '2020', deforestation: 207.0, municipality: 'Neopolis', state: 'Sergipe' },
  {year : '2021', deforestation: 207.1, municipality: 'Neopolis', state: 'Sergipe' },
  {year : '2022', deforestation: 207.6, municipality: 'Neopolis', state: 'Sergipe' },
  {year : '2018', deforestation: 0.1, municipality: 'Nossa Senhora Aparecida', state: 'Sergipe' },
  {year : '2019', deforestation: 0.1, municipality: 'Nossa Senhora Aparecida', state: 'Sergipe' },
  {year : '2020', deforestation: 0.1, municipality: 'Nossa Senhora Aparecida', state: 'Sergipe' },
  {year : '2021', deforestation: 0.1, municipality: 'Nossa Senhora Aparecida', state: 'Sergipe' },
  {year : '2022', deforestation: 0.1, municipality: 'Nossa Senhora Aparecida', state: 'Sergipe' },
  {year : '2018', deforestation: 416.3, municipality: 'Nossa Senhora das Dores', state: 'Sergipe' },
  {year : '2019', deforestation: 416.4, municipality: 'Nossa Senhora das Dores', state: 'Sergipe' },
  {year : '2020', deforestation: 417.9, municipality: 'Nossa Senhora das Dores', state: 'Sergipe' },
  {year : '2021', deforestation: 418.3, municipality: 'Nossa Senhora das Dores', state: 'Sergipe' },
  {year : '2022', deforestation: 418.5, municipality: 'Nossa Senhora das Dores', state: 'Sergipe' },
  {year : '2018', deforestation: 112.9, municipality: 'Nossa Senhora do Socorro', state: 'Sergipe' },
  {year : '2019', deforestation: 112.9, municipality: 'Nossa Senhora do Socorro', state: 'Sergipe' },
  {year : '2020', deforestation: 113.5, municipality: 'Nossa Senhora do Socorro', state: 'Sergipe' },
  {year : '2021', deforestation: 113.5, municipality: 'Nossa Senhora do Socorro', state: 'Sergipe' },
  {year : '2022', deforestation: 113.7, municipality: 'Nossa Senhora do Socorro', state: 'Sergipe' },
  {year : '2018', deforestation: 186.5, municipality: 'Pacatuba', state: 'Sergipe' },
  {year : '2019', deforestation: 186.8, municipality: 'Pacatuba', state: 'Sergipe' },
  {year : '2020', deforestation: 188.2, municipality: 'Pacatuba', state: 'Sergipe' },
  {year : '2021', deforestation: 188.4, municipality: 'Pacatuba', state: 'Sergipe' },
  {year : '2022', deforestation: 188.6, municipality: 'Pacatuba', state: 'Sergipe' },
  {year : '2018', deforestation: 31.6, municipality: 'Pedrinhas', state: 'Sergipe' },
  {year : '2019', deforestation: 31.6, municipality: 'Pedrinhas', state: 'Sergipe' },
  {year : '2020', deforestation: 31.6, municipality: 'Pedrinhas', state: 'Sergipe' },
  {year : '2021', deforestation: 31.7, municipality: 'Pedrinhas', state: 'Sergipe' },
  {year : '2022', deforestation: 31.7, municipality: 'Pedrinhas', state: 'Sergipe' },
  {year : '2018', deforestation: 108.6, municipality: 'Pirambu', state: 'Sergipe' },
  {year : '2019', deforestation: 108.7, municipality: 'Pirambu', state: 'Sergipe' },
  {year : '2020', deforestation: 109.0, municipality: 'Pirambu', state: 'Sergipe' },
  {year : '2021', deforestation: 109.0, municipality: 'Pirambu', state: 'Sergipe' },
  {year : '2022', deforestation: 109.1, municipality: 'Pirambu', state: 'Sergipe' },
  {year : '2018', deforestation: 81.4, municipality: 'Propria', state: 'Sergipe' },
  {year : '2019', deforestation: 81.4, municipality: 'Propria', state: 'Sergipe' },
  {year : '2020', deforestation: 81.5, municipality: 'Propria', state: 'Sergipe' },
  {year : '2021', deforestation: 81.5, municipality: 'Propria', state: 'Sergipe' },
  {year : '2022', deforestation: 81.5, municipality: 'Propria', state: 'Sergipe' },
  {year : '2018', deforestation: 146.3, municipality: 'Riachao do Dantas', state: 'Sergipe' },
  {year : '2019', deforestation: 146.6, municipality: 'Riachao do Dantas', state: 'Sergipe' },
  {year : '2020', deforestation: 148.1, municipality: 'Riachao do Dantas', state: 'Sergipe' },
  {year : '2021', deforestation: 148.2, municipality: 'Riachao do Dantas', state: 'Sergipe' },
  {year : '2022', deforestation: 148.3, municipality: 'Riachao do Dantas', state: 'Sergipe' },
  {year : '2018', deforestation: 62.8, municipality: 'Riachuelo', state: 'Sergipe' },
  {year : '2019', deforestation: 62.9, municipality: 'Riachuelo', state: 'Sergipe' },
  {year : '2020', deforestation: 63.3, municipality: 'Riachuelo', state: 'Sergipe' },
  {year : '2021', deforestation: 63.3, municipality: 'Riachuelo', state: 'Sergipe' },
  {year : '2022', deforestation: 63.4, municipality: 'Riachuelo', state: 'Sergipe' },
  {year : '2018', deforestation: 114.8, municipality: 'Ribeiropolis', state: 'Sergipe' },
  {year : '2019', deforestation: 114.8, municipality: 'Ribeiropolis', state: 'Sergipe' },
  {year : '2020', deforestation: 115.5, municipality: 'Ribeiropolis', state: 'Sergipe' },
  {year : '2021', deforestation: 115.6, municipality: 'Ribeiropolis', state: 'Sergipe' },
  {year : '2022', deforestation: 115.7, municipality: 'Ribeiropolis', state: 'Sergipe' },
  {year : '2018', deforestation: 91.7, municipality: 'Rosario do Catete', state: 'Sergipe' },
  {year : '2019', deforestation: 91.7, municipality: 'Rosario do Catete', state: 'Sergipe' },
  {year : '2020', deforestation: 91.8, municipality: 'Rosario do Catete', state: 'Sergipe' },
  {year : '2021', deforestation: 91.8, municipality: 'Rosario do Catete', state: 'Sergipe' },
  {year : '2022', deforestation: 91.8, municipality: 'Rosario do Catete', state: 'Sergipe' },
  {year : '2018', deforestation: 204.2, municipality: 'Salgado', state: 'Sergipe' },
  {year : '2019', deforestation: 204.2, municipality: 'Salgado', state: 'Sergipe' },
  {year : '2020', deforestation: 205.8, municipality: 'Salgado', state: 'Sergipe' },
  {year : '2021', deforestation: 206.1, municipality: 'Salgado', state: 'Sergipe' },
  {year : '2022', deforestation: 206.4, municipality: 'Salgado', state: 'Sergipe' },
  {year : '2018', deforestation: 210.1, municipality: 'Santa Luzia do Itanhy', state: 'Sergipe' },
  {year : '2019', deforestation: 211.2, municipality: 'Santa Luzia do Itanhy', state: 'Sergipe' },
  {year : '2020', deforestation: 212.5, municipality: 'Santa Luzia do Itanhy', state: 'Sergipe' },
  {year : '2021', deforestation: 212.7, municipality: 'Santa Luzia do Itanhy', state: 'Sergipe' },
  {year : '2022', deforestation: 213.1, municipality: 'Santa Luzia do Itanhy', state: 'Sergipe' },
  {year : '2018', deforestation: 52.4, municipality: 'Santa Rosa de Lima', state: 'Sergipe' },
  {year : '2019', deforestation: 52.4, municipality: 'Santa Rosa de Lima', state: 'Sergipe' },
  {year : '2020', deforestation: 52.9, municipality: 'Santa Rosa de Lima', state: 'Sergipe' },
  {year : '2021', deforestation: 52.9, municipality: 'Santa Rosa de Lima', state: 'Sergipe' },
  {year : '2022', deforestation: 53.1, municipality: 'Santa Rosa de Lima', state: 'Sergipe' },
  {year : '2018', deforestation: 34.0, municipality: 'Santana do Sao Francisco', state: 'Sergipe' },
  {year : '2019', deforestation: 34.0, municipality: 'Santana do Sao Francisco', state: 'Sergipe' },
  {year : '2020', deforestation: 34.0, municipality: 'Santana do Sao Francisco', state: 'Sergipe' },
  {year : '2021', deforestation: 34.0, municipality: 'Santana do Sao Francisco', state: 'Sergipe' },
  {year : '2022', deforestation: 34.1, municipality: 'Santana do Sao Francisco', state: 'Sergipe' },
  {year : '2018', deforestation: 144.8, municipality: 'Santo Amaro das Brotas', state: 'Sergipe' },
  {year : '2019', deforestation: 145.2, municipality: 'Santo Amaro das Brotas', state: 'Sergipe' },
  {year : '2020', deforestation: 145.7, municipality: 'Santo Amaro das Brotas', state: 'Sergipe' },
  {year : '2021', deforestation: 145.9, municipality: 'Santo Amaro das Brotas', state: 'Sergipe' },
  {year : '2022', deforestation: 146.6, municipality: 'Santo Amaro das Brotas', state: 'Sergipe' },
  {year : '2018', deforestation: 260.5, municipality: 'Sao Cristovao', state: 'Sergipe' },
  {year : '2019', deforestation: 260.9, municipality: 'Sao Cristovao', state: 'Sergipe' },
  {year : '2020', deforestation: 263.4, municipality: 'Sao Cristovao', state: 'Sergipe' },
  {year : '2021', deforestation: 263.7, municipality: 'Sao Cristovao', state: 'Sergipe' },
  {year : '2022', deforestation: 264.0, municipality: 'Sao Cristovao', state: 'Sergipe' },
  {year : '2018', deforestation: 40.8, municipality: 'Sao Domingos', state: 'Sergipe' },
  {year : '2019', deforestation: 41.0, municipality: 'Sao Domingos', state: 'Sergipe' },
  {year : '2020', deforestation: 41.6, municipality: 'Sao Domingos', state: 'Sergipe' },
  {year : '2021', deforestation: 41.8, municipality: 'Sao Domingos', state: 'Sergipe' },
  {year : '2022', deforestation: 41.9, municipality: 'Sao Domingos', state: 'Sergipe' },
  {year : '2018', deforestation: 79.4, municipality: 'Sao Francisco', state: 'Sergipe' },
  {year : '2019', deforestation: 79.4, municipality: 'Sao Francisco', state: 'Sergipe' },
  {year : '2020', deforestation: 79.4, municipality: 'Sao Francisco', state: 'Sergipe' },
  {year : '2021', deforestation: 79.4, municipality: 'Sao Francisco', state: 'Sergipe' },
  {year : '2022', deforestation: 79.6, municipality: 'Sao Francisco', state: 'Sergipe' },
  {year : '2018', deforestation: 74.9, municipality: 'Sao Miguel do Aleixo', state: 'Sergipe' },
  {year : '2019', deforestation: 74.9, municipality: 'Sao Miguel do Aleixo', state: 'Sergipe' },
  {year : '2020', deforestation: 75.4, municipality: 'Sao Miguel do Aleixo', state: 'Sergipe' },
  {year : '2021', deforestation: 75.5, municipality: 'Sao Miguel do Aleixo', state: 'Sergipe' },
  {year : '2022', deforestation: 75.6, municipality: 'Sao Miguel do Aleixo', state: 'Sergipe' },
  {year : '2018', deforestation: 145.0, municipality: 'Siriri', state: 'Sergipe' },
  {year : '2019', deforestation: 145.1, municipality: 'Siriri', state: 'Sergipe' },
  {year : '2020', deforestation: 145.3, municipality: 'Siriri', state: 'Sergipe' },
  {year : '2021', deforestation: 145.4, municipality: 'Siriri', state: 'Sergipe' },
  {year : '2022', deforestation: 145.6, municipality: 'Siriri', state: 'Sergipe' },
  {year : '2018', deforestation: 27.4, municipality: 'Telha', state: 'Sergipe' },
  {year : '2019', deforestation: 27.4, municipality: 'Telha', state: 'Sergipe' },
  {year : '2020', deforestation: 27.4, municipality: 'Telha', state: 'Sergipe' },
  {year : '2021', deforestation: 27.4, municipality: 'Telha', state: 'Sergipe' },
  {year : '2022', deforestation: 27.4, municipality: 'Telha', state: 'Sergipe' },
  {year : '2018', deforestation: 32.6, municipality: 'Tomar do Geru', state: 'Sergipe' },
  {year : '2019', deforestation: 32.7, municipality: 'Tomar do Geru', state: 'Sergipe' },
  {year : '2020', deforestation: 32.7, municipality: 'Tomar do Geru', state: 'Sergipe' },
  {year : '2021', deforestation: 32.7, municipality: 'Tomar do Geru', state: 'Sergipe' },
  {year : '2022', deforestation: 32.7, municipality: 'Tomar do Geru', state: 'Sergipe' },
  {year : '2018', deforestation: 110.5, municipality: 'Umbauba', state: 'Sergipe' },
  {year : '2019', deforestation: 110.8, municipality: 'Umbauba', state: 'Sergipe' },
  {year : '2020', deforestation: 111.2, municipality: 'Umbauba', state: 'Sergipe' },
  {year : '2021', deforestation: 111.3, municipality: 'Umbauba', state: 'Sergipe' },
  {year : '2022', deforestation: 111.4, municipality: 'Umbauba', state: 'Sergipe' }
  ];
  
  export default data;